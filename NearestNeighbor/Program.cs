﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace NearestNeighbor
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @".\points.txt";

            // Create a points file
            if (!File.Exists(path))
            {
                StreamWriter sw = File.CreateText(path);
            }

            // Main menu
            Console.WriteLine(@"
╔════════════════════════════════════════════════════╗
║ Travelling Salesman Problem using Nearest Neighbor ║ 
╠────────────────────────────────────────────────────╣
║              Author: Łukasz Kondracki              ║ 
╚════════════════════════════════════════════════════╝

╔════════════════════════════════════════════════════╗
║       Press ENTER to calculate the distance        ║
╠════════════════════════════════════════════════════╝");

            Console.Write("║");
            Console.ReadLine();

            // Read all cities to an array of strings
            string[] pointStringsFromFile = File.ReadAllLines(path);
            
            // Check if file isn't empty
            if (pointStringsFromFile.Count() == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\nEMPTY FILE. ABORTING");
                Console.ReadLine();
                Environment.Exit(0);
            }

            // Create a list of points
            List<Point> pointsList = new List<Point>();
            // Create a list of points, in route order
            List<Point> routeList = new List<Point>();


            // Move all cities from the file to a list
            foreach (string s in pointStringsFromFile)
            {
                string[] pointsStringArr = s.Split(',');

                pointsList.Add(new Point(Int32.Parse(pointsStringArr[1]), Int32.Parse(pointsStringArr[2]), pointsStringArr[0]));
            }


            // Calculate the route
            Point currentPoint = pointsList[0];
            Point nearestPoint = new Point();
            while (pointsList.Count != 0)
            {
                // Output points remaining and current shortest route
                Console.WriteLine("╠───────────────────────────────────────────────────────────");
                Console.Write    ("╠─ Points remaining to travel through:     ");
                foreach (Point m in pointsList)
                {
                    Console.Write(m.Name + " ");
                }
                Console.WriteLine();
                Console.Write    ("╠─ Route so far:                           ");
                foreach (Point m in routeList)
                {
                    Console.Write(m.Name + " ");
                }
                Console.WriteLine();

                pointsList.Remove(currentPoint);
                routeList.Add(currentPoint);

                double closestPointDistance = Double.PositiveInfinity;
                foreach (Point p in pointsList)
                {
                    if (currentPoint.Distance(p) < closestPointDistance)
                    {
                        closestPointDistance = currentPoint.Distance(p);
                        nearestPoint = p;
                    }

                    currentPoint = nearestPoint;
                }
            }

            // Calculate and sisplay shortest routes
            double shortestRoute = Route.ShortestRouteLegth(routeList);
            double shortestRouteReturn = shortestRoute + routeList[0].Distance(routeList[routeList.Count - 1]);

            Console.WriteLine("╚───────────────────────────────────────────────────────────");

            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine("╔═══════════════════════════════════════════════════════════");

            // Shortest route with return - points
            Console.Write    ("╠─ Shortest route - with return:           ");
            foreach (Point m in routeList)
            {
                Console.Write(m.Name + " ");
            }
            Console.Write(routeList[0].Name);

            Console.WriteLine();

            // Shortest route with return - length
            Console.WriteLine("╠─ Route length - with return:             " + String.Format("{0:00.00}", shortestRouteReturn));

            Console.WriteLine("╠═══════════════════════════════════════════════════════════");

            // Shortest route without return - points
            Console.Write    ("╠─ Shortest route - without return:        ");
            foreach (Point m in routeList)
            {
                Console.Write(m.Name + " ");
            }

            Console.WriteLine();

            // Shortest route without return - length
            Console.WriteLine("╠─ Route legth - without return:           " + String.Format("{0:00.00}", shortestRoute));

            Console.Write    ("╚═══════════════════════════════════════════════════════════");

            Console.Read();
        }
    }

    // Class containing object Point
    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Name { get; set; }

        // Constructor
        public Point(int x, int y, string name)
        {
            X = x;
            Y = y;
            Name = name;
        }

        // Argument-less constructor
        public Point() { }

        // Method calculating distance
        public double Distance(Point point)
        {
            // Delta X squared
            double deltaXSquared = Math.Pow(X - point.X, 2);
            // Delta Y squared
            double deltaYSquared = Math.Pow(Y - point.Y, 2);
            // Root of squares
            double result = Math.Sqrt(deltaXSquared + deltaYSquared);

            return result;
        }
    }

    // Class containing object Route
    class Route
    {
        public Point PointA { get; set; }
        public Point PointB { get; set; }
        public double Distance { get; set; }

        // Constructor of a route from A to B
        public Route (Point pointA, Point pointB)
        {
            PointA = pointA;
            PointB = pointB;

            Distance = pointA.Distance(pointB);
        }

        // Method that sums up the total legth of a route
        public static double ShortestRouteLegth (List<Point> points)
        {
            double result = 0;

            for (int i = 0; i < points.Count() - 1; i++)
            {
                result = result + points[i].Distance(points[i + 1]);
            }

            return result;
        }
    }
}
